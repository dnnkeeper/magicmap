﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapReveal : MonoBehaviour {

    public string paramName = "_Distance";

    public float speed = 1f;

    public float amount = 5f;

    // Update is called once per frame
    void Update () {

        GetComponent<Renderer>().material.SetFloat(paramName, Mathf.Sin(Time.time*speed) * amount + amount*0.5f);
        
	}
}
